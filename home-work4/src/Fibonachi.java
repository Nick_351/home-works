public class Fibonachi {
    public static int getNumberOfFibonachi(int number){
        int numberTotal=1;
        int numberRight=1;
        int numberLeft=1;

        for(int i=3; i<=number; i++){
            numberTotal = numberLeft+numberRight;
            numberLeft = numberRight;
            numberRight = numberTotal;
        }
        return numberTotal;
    }
    public static int getNumberOfFibonachiRec(int number){
        if(number<3){
            return 1;
        }
        else {
            return getNumberOfFibonachiRec(number-1)+getNumberOfFibonachiRec(number-2);
        }
    }

    public static void main(String[] args) {
        System.out.println(getNumberOfFibonachi(10));
        System.out.println(getNumberOfFibonachiRec(10));
    }
}
