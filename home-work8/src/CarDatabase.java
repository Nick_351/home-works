public interface CarDatabase {
    void takeAllCars();
    String searchByID();
    void addNewCar();
    void editCar();
    void deleteCar();
}

