import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.nio.file.*;
import java.util.Scanner;

@Getter
@Setter
public class MyCarDatabase implements CarDatabase {
    private Car car;
    private FileAddresses addresses = new FileAddresses();

    @Override
    public void takeAllCars() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getAddresses().getFileToReadAndWrite()));
            String stroke = bufferedReader.readLine();
            while (stroke != null) {
                System.out.print(stroke + " \n");
                stroke = bufferedReader.readLine();
            }
            System.out.println();
        } catch (IOException e) {
            System.out.println("Проблема с IO");
        }
    }

    @Override
    public String searchByID() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите ID автомобиля: ");
        int needID = scan.nextInt();
        boolean result = false;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getAddresses().getFileToReadAndWrite()));
            String stroke = bufferedReader.readLine();
            while (stroke != null) {
                result = stroke.contains("ID - " + Integer.toString(needID));
                if (result == true) {
                    System.out.println(stroke);
                    bufferedReader.close();
                    return stroke;
                }
                stroke = bufferedReader.readLine();
            }
            if (result == false) {
                System.out.println("Автомобиль с таким ID не найден в базе!");
            }
        } catch (IOException e) {
            System.out.println("Проблема с IO");
        }
        return null;
    }

    @Override
    public void addNewCar() {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(getAddresses().getFileToReadAndWrite(), true));
        } catch (IOException e) {
            System.out.println("Ошибка с IO");
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите марку автомобиля: ");
        String brand = scanner.nextLine();
        System.out.println("Введите модель автомобилы: ");
        String model = scanner.nextLine();
        System.out.println("Введите цвет автомобилы: ");
        String color = scanner.nextLine();
        System.out.println("Введите количество лошадиных сил: ");
        String enginePower = scanner.nextLine();
        try {
            car = new Car(brand, model, color, enginePower);
            bufferedWriter.write("ID - " + Car.getID() + " ");
            bufferedWriter.write(brand + " ");
            bufferedWriter.write(model + " ");
            bufferedWriter.write(color + " ");
            bufferedWriter.write(enginePower + "\n");
            bufferedWriter.flush();
            System.out.println("Автомобиль успешно добавлен в базу! Его ID - " + (Car.getID()));
        } catch (IOException e) {
            System.out.println("Ошибка с IO");
        }
    }
    @Override
    public void editCar() {
        try {
            File temp = new File(getAddresses().getFileToWriteTemp());
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getAddresses().getFileToReadAndWrite()));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(getAddresses().getFileToWriteTemp()));
            boolean result = false;
            String editStroke = null;
            editStroke = searchByID();
            if (editStroke == null) {
                System.out.println("Автомобиля с таким ID нет в базе! Программа закончила работу!");
            }
            else {
                Scanner scan2 = new Scanner(System.in);
                System.out.println("Введите новые данные автомобиля: ");
                String strokeToChange = scan2.nextLine();
                String currentStroke;
                while ((currentStroke = bufferedReader.readLine()) != null) {
                    if (!currentStroke.equals(editStroke)) {
                        bufferedWriter.write(currentStroke);
                        bufferedWriter.newLine();
                    }
                    if (currentStroke.equals(editStroke)) {
                        bufferedWriter.write(strokeToChange);
                        bufferedWriter.newLine();
                        System.out.println("Данные автомобиля успешно отредактированы!");
                    }
                }
                bufferedWriter.flush();
                bufferedWriter.close();
                bufferedReader.close();
                Path sourse = Paths.get(getAddresses().getFileToReadAndWrite());
                Files.delete(Paths.get(getAddresses().getFileToReadAndWrite()));
                temp.renameTo(sourse.toFile());
            }
        } catch (NoSuchFileException e) {
            System.out.println("NoSuchFileException e");
        }
        catch (DirectoryNotEmptyException e){
            System.out.println("DirectoryNotEmptyException e");
        }
        catch (IOException e){
            System.out.println("Проблема с IO");
        }
    }

    @Override
    public void deleteCar() {
        try {
            File temp = new File(getAddresses().getFileToWriteTemp());
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getAddresses().getFileToReadAndWrite()));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(getAddresses().getFileToWriteTemp()));
            boolean result = false;
            String strokeToRemove = null;
            String currentStroke = null;
            strokeToRemove = searchByID();
            if (strokeToRemove == null) {
                System.out.println("Автомобиля с таким ID нет в базе! Программа закончила работу!");
                return;
            }
            else {
                while ((currentStroke = bufferedReader.readLine()) != null) {
                    if (!currentStroke.equals(strokeToRemove)) {
                        bufferedWriter.write(currentStroke);
                        bufferedWriter.newLine();
                    }
                }
                bufferedWriter.flush();
            }
            bufferedWriter.close();
            bufferedReader.close();
            bufferedReader.close();
            Path sourse = Paths.get(getAddresses().getFileToReadAndWrite());
            Files.delete(Paths.get(getAddresses().getFileToReadAndWrite()));
            temp.renameTo(sourse.toFile());
            System.out.println("Данные автомобиля успешно удалены!");
        } catch (IOException e) {
            System.out.println("Проблема с IO");
        }
    }

    public void choosingAction() {
        System.out.println("Перечень режимов работы: ");
        System.out.println("1 - вывести список всех машин из базы данных");
        System.out.println("2 - найти машину по её ID");
        System.out.println("3 - добавить новую машину");
        System.out.println("4 - редактировать машину");
        System.out.println("5 - удалить машину");
        System.out.print("Выберите режим работы: ");
        Scanner scanner = new Scanner(System.in);
        int result = scanner.nextInt();
        if (result == 1) {
            takeAllCars();
        } else if (result == 2) {
            searchByID();
        } else if (result == 3) {
            addNewCar();
        } else if (result == 4) {
            editCar();
        } else if (result == 5) {
            deleteCar();
        } else {
            System.out.println("ОШИБКА ВВОДА");
        }
    }
}

