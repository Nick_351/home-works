import lombok.Getter;
import lombok.Setter;
import java.io.*;
@Getter
@Setter
public class Car {
    private static int ID;
    private String brand;
    private String model;
    private String color;
    private String enginePower;

    public static int getID() {
        return ID;
    }

    public static void setID(int ID) {
        Car.ID = ID;
    }

    public Car(String brand, String model, String color, String enginePower) throws IOException {
        currentID();
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.enginePower = enginePower;
    }

    private static void currentID() {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader("resourses/ID.txt"));
            String stroke = bufferedReader.readLine();
            if(stroke != null){
                int result = Integer.valueOf(stroke);
                if (result == 0){
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resourses/ID.txt", false));
                    bufferedWriter.write("2");
                    bufferedWriter.flush();
                    setID(1);
                }
                if (result != 0){
                    setID(result);
                    System.out.println(getID());
                    if (result > 0){
                        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resourses/ID.txt", false));
                        int result2 = Integer.valueOf(stroke) + 1;
                        bufferedWriter.write(Integer.toString(result2));
                        bufferedWriter.flush();
                    }
                }

            }
        }
        catch (IOException e){
            System.out.println("Проблема с IO");
        }

    }
}

