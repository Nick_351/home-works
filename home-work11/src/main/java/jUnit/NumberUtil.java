package jUnit;

public class NumberUtil {
    public static int largestDivisor(int firstNumber, int secondNumber){
        int result = 0;
        firstNumber = Math.abs(firstNumber);
        secondNumber = Math.abs(secondNumber);
        if (firstNumber == 0 || secondNumber == 0){
            throw new ArithmeticException("Одно из введенных чисел равно 0");
        }
        else if(firstNumber == secondNumber){
            result = firstNumber;
        }
        else if (firstNumber < secondNumber){
            for(int i = firstNumber; i <= secondNumber; i--){
                if((firstNumber % i == 0) && (secondNumber % i == 0)){
                    result = i;
                    break;
                }
            }
        }
        else if(firstNumber > secondNumber){
            for(int i = secondNumber; i <= firstNumber; i--){
                if((firstNumber % i == 0) && (secondNumber % i == 0)){
                    result = i;
                    break;
                }
            }
        }
        return result;
    }
}
