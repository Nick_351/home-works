package jUnit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class NumberUtilTest {
    @Test
    @DisplayName("Проверка метода largestDivisor()")
    void largestDivisor() {
        assertEquals(5, NumberUtil.largestDivisor(15,5));
        assertEquals(9, NumberUtil.largestDivisor(27,18));
        assertEquals(6, NumberUtil.largestDivisor(12,18));
        assertEquals(100, NumberUtil.largestDivisor(100,100));
        assertEquals(10, NumberUtil.largestDivisor(-10,100));
        assertThrows(ArithmeticException.class, () -> NumberUtil.largestDivisor(0,23));
    }
}