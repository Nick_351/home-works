package com.example.service;

import com.example.dto.ProductDto;
import com.example.model.Product;

import java.util.List;

public interface ProductService {

    void saveProduct(ProductDto productDto);

    List<Product> getAllProducts();
    Product getByID(Long id);
    void deleteByID(Long id);
    void editByID(Long id, ProductDto productDto);
}
