package com.example.service;

import com.example.dto.ProductDto;
import com.example.model.Product;
import com.example.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void saveProduct(ProductDto productDto) {
        Product product = Product.builder()
                .name(productDto.getName())
                .brand(productDto.getBrand())
                .price(productDto.getPrice())
                .code(productDto.getCode())
                .build();

        productRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
//        return productRepository.getAllProducts();
        return productRepository.findAll();
    }

    @Override
    public Product getByID(Long id) {
//        Optional<Product> product = Optional.ofNullable(productRepository.getByID(id));
        Optional<Product> product = Optional.ofNullable(productRepository.findById(id).get());
        return product.orElse(Product.builder().build());
    }

    @Override
    public void deleteByID(Long id) {
//        productRepository.deleteProduct(id);
        productRepository.delete(productRepository.findById(id).get());
    }

    @Override
    public void editByID(Long id, ProductDto productDto) {
        productRepository.delete(productRepository.findById(id).get());
        Product product = Product.builder()
                .name(productDto.getName())
                .brand(productDto.getBrand())
                .price(productDto.getPrice())
                .code(productDto.getCode())
                .id(id)
                .build();
//        productRepository.editProduct(product, id);
        productRepository.save(product);
    }
}
