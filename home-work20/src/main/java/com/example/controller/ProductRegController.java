package com.example.controller;

import com.example.dto.ProductDto;
import com.example.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/registrationProduct")
@RequiredArgsConstructor
public class ProductRegController {

    private final ProductService productService;
    @RequestMapping(method = RequestMethod.GET)
    public String getRegistrationProductPage(){
        return "registrationProduct";
    }
//    @PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String registrationProduct(ProductDto productDto){
        productService.saveProduct(productDto);
        return "redirect:/products";
    }
}
