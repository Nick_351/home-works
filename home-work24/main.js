const http = require('http')
const fs = require('fs')

let server = http.createServer((request, response) => {
    response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
    if (request.url == '/') {
        fs.createReadStream('./pages/main_page.html').pipe(response)
    } else if (request.url == '/sign_in') {
        fs.createReadStream('./pages/sign_in.html').pipe(response)
    } else  if (request.url == '/reg_product'){
        fs.createReadStream('./pages/reg_product.html').pipe(response)
    } else  if (request.url == '/help'){
        fs.createReadStream('./pages/help.html').pipe(response)
    } else  if (request.url == '/address'){
        fs.createReadStream('./pages/address.html').pipe(response)
    }
})

const PORT = 9900
const HOST = 'localhost'

server.listen(PORT, HOST, () => console.log('Node.js server started'))
