package org.controllers.repository;


import org.controllers.model.Product;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {


    private DataBaseHiber dataBaseHiber;

    @Autowired
    public ProductRepositoryImpl(DataBaseHiber dataBaseHiber) {
        this.dataBaseHiber = dataBaseHiber;
    }

    @Override
    public void save(Product product) {
        try(Session session = dataBaseHiber.getSession()) {
            session.save(product);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> getAllProducts() {
        try(Session session = dataBaseHiber.getSession()) {
            NativeQuery<Product> query = session.createNativeQuery("select * from product", Product.class);
            return query.getResultList();
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }
}
