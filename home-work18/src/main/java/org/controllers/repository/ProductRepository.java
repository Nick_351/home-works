package org.controllers.repository;
import org.controllers.model.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository {

    void save(Product product);

    List<Product> getAllProducts();
}
