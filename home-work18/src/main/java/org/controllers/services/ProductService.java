package org.controllers.services;

import org.controllers.dto.ProductDto;
import org.controllers.model.Product;

import java.util.List;

public interface ProductService {

    void saveAccount(ProductDto productDto);

    List<Product> getAllProducts();
}
