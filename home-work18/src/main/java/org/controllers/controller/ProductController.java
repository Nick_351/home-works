package org.controllers.controller;

import lombok.RequiredArgsConstructor;
import org.controllers.services.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/registrationProduct")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    public String getRegistrationProductPage(){
        return "registrationProduct";
    }

}
