package src;

public class Main {

    public static void main(String[] args) {

        FaceControl faceControlVasiliy = new FaceControl(2);
        Factory ourFactory = Factory.builder()
                .faceControl(faceControlVasiliy)
                .name("Кировский завод")
                .build();

        Worker worker1 = Worker.builder()
                .nameWorker("Ваня")
                .factory(ourFactory)
                .build();
        Worker worker2 = Worker.builder()
                .nameWorker("Витек")
                .factory(ourFactory)
                .build();
        Worker worker3 = Worker.builder()
                .nameWorker("Колян")
                .factory(ourFactory)
                .build();
        Worker worker4 = Worker.builder()
                .nameWorker("Жорик")
                .factory(ourFactory)
                .build();
        Worker worker5 = Worker.builder()
                .nameWorker("Степан")
                .factory(ourFactory)
                .build();
        Worker worker6 = Worker.builder()
                .nameWorker("Толян")
                .factory(ourFactory)
                .build();
    }
}
