package src;

import java.util.concurrent.Semaphore;
public class FaceControl {
    private Semaphore semaphore;

    public FaceControl(int workerCount) {
        this.semaphore = new Semaphore(workerCount);
    }

    public void passingOfWorker(){
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public void goHome(){
        semaphore.release();
    }
}
