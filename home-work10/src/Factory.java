package src;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class Factory {
    private FaceControl faceControl;
    private String name;

    public void passingByFaceControl(){
        getFaceControl().passingOfWorker();
    }
    public void goHome(){
        getFaceControl().goHome();
    }
}
