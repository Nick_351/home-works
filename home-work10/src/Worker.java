package src;

import lombok.Builder;

@Builder
public class Worker extends Thread{
    private String nameWorker;
    private Factory factory;
    public Worker(String nameWorker, Factory factory){
        this.nameWorker = nameWorker;
        this.factory = factory;
        start();
    }
    @Override
    public void run() {
        System.out.println("Рабочий по имени " + nameWorker + " пришел на " + factory.getName());
        doSomething(500);
        System.out.println("Рабочий по имени " + nameWorker + " ждет пропуск");
        doSomething(500);
        factory.passingByFaceControl();
        System.out.println("Рабочий по имени " + nameWorker + " получил пропуск");
        doSomething(500);
        System.out.println("Рабочий по имени " + nameWorker + " работает");
        doSomething(5000);
        factory.goHome();
        System.out.println("Рабочий по имени " + nameWorker + " отдал пропуск");
        doSomething(500);
        System.out.println("Рабочий по имени " + nameWorker + " пошел домой");
    }
    private void doSomething(int count){
        try {
            currentThread().sleep(count);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
