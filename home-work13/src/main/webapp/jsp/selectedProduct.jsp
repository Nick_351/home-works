<%@ page import="java.util.ArrayList" %>
<%@ page import="models.Product" %><%--
  Created by IntelliJ IDEA.
  User: j351
  Date: 21.09.2023
  Time: 12:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products");%>
<html>
<head>
    <title>Данные продукта</title>
</head>
<body>
<div align="center">
    <h2>Код продукта: <%=products.get(Integer.parseInt((String) request.getAttribute("id"))-1).getCode()%></h2>
    <h2>Название: <%=products.get(Integer.parseInt((String) request.getAttribute("id"))-1).getName()%></h2>
    <h2>Производитель: <%=products.get(Integer.parseInt((String) request.getAttribute("id"))-1).getBrand()%></h2>
    <h2>Цена: <%=products.get(Integer.parseInt((String) request.getAttribute("id"))-1).getPrice()%></h2>
</div>

</body>
</html>
