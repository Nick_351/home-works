<%@ page import="java.util.ArrayList" %>
<%@ page import="models.User" %><%--
  Created by IntelliJ IDEA.
  User: j351
  Date: 21.09.2023
  Time: 12:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ArrayList<User> users = (ArrayList<User>) request.getAttribute("users");%>
<html>
<head>
    <title>Данные пользователя</title>
</head>
<body>
<div align="center">
    <h2>Идентификатор пользователя: <%=request.getAttribute("id")%></h2>
    <h2>Имя: <%=users.get(Integer.parseInt((String) request.getAttribute("id"))).getFirstName()%></h2>
    <h2>Фамилия: <%=users.get(Integer.parseInt((String) request.getAttribute("id"))).getLastName()%></h2>
    <h2>Возраст: <%=users.get(Integer.parseInt((String) request.getAttribute("id"))).getAge()%></h2>
    <h2>Местоположение: <%=users.get(Integer.parseInt((String) request.getAttribute("id"))).getLocation()%></h2>
    <h2>E-mail: <%=users.get(Integer.parseInt((String) request.getAttribute("id"))).getEmail()%></h2>
</div>

</body>
</html>
