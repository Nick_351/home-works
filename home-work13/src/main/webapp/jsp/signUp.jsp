<%--
  Created by IntelliJ IDEA.
  User: j351
  Date: 19.09.2023
  Time: 14:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Страница регистрации</title>
</head>
<body>
<form action="/signUp" method="post" align="center" valign="bottom">
    <label for="firstName">Enter your firstName</label>
    <input id="firstName" name="firstName" placeholder="Your first name">
    <br>
    <label for="lastName">Enter your lastName</label>
    <input id="lastName" name="lastName" placeholder="Your last name">
    <br>
    <label for="age">Введите Ваш возраст</label>
    <input id="age" name="age" placeholder="Ваш возраст">
    <br>
    <label for="location">Введите Ваше местоположение</label>
    <input id="location" name="location" placeholder="Ваше местоположение">
    <br>
    <label for="email">Enter your email</label>
    <input id="email" type="email" name="email" placeholder="Your email">
    <br>
    <label for="password">Enter your password</label>
    <input id="password" type="password" name="password" placeholder="Your password">
    <input type="submit" value="Sign Up!">
</form>
</body>
</html>
