<%@ page import="java.util.ArrayList" %>
<%@ page import="models.Product" %><%--
  Created by IntelliJ IDEA.
  User: j351
  Date: 21.09.2023
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products");%>
<html>
<head>
    <title>Продукты</title>
</head>
<body>
<h1 align="center">Список всех продуктов</h1>
<%for(Product product: products){%>
<form  method="post" align="center"><a href="/selectedProduct?id=<%=product.getId()%>"
                                                                   style="font-size: 22px; text-decoration: none">
    <h3><%=product.getName() + " " + "\"" + product.getBrand() + "\""%></h3>
</a>
</form>
<%}%>

</body>
</html>
