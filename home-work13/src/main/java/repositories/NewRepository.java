package repositories;

import models.News;
import java.util.ArrayList;

public interface NewRepository {
    ArrayList<News> loadAllNews();
}
