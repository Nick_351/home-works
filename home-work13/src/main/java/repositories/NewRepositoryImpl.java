package repositories;

import models.News;

import java.util.ArrayList;

public class NewRepositoryImpl implements NewRepository {
    public static ArrayList<News> news;
    public ArrayList<News> loadAllNews() {
        news = new ArrayList<News>();
        news.add(new News(1L,"Папа Римский Франциск принял ислам и отказался от свинины","15 мая 2023 года"));
        news.add(new News(2l,"Инопланетянин 10 лет работал учителем труда в Челябинске","20 июня 2023 года"));
        news.add(new News(3L,"Как быть? Шаверма опять подорожала!","18 июля 2023 года"));
        news.add(new News(4l,"Актуально ли учить Java в 2023 году?","7 августа 2023 года"));
        news.add(new News(5L,"Как создать популярную фейковую новость","22 сентября 2023 года"));
        return news;
    }
}
