package repositories;

import models.Product;

import java.util.ArrayList;

public interface ProductRepository {
    ArrayList<Product> loadAllProducts();
}
