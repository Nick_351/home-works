package repositories;

import models.Contact;
import java.util.ArrayList;

public class ContactRepositoryImpl implements ContactRepository {
    public static ArrayList<Contact> contacts;

    public ArrayList<Contact> loadAllContacts() {
        contacts = new ArrayList<Contact>();
        contacts.add(new Contact(1L,"Самокат","Москва, ул. Ленина, д.25","http://samokat.ru",
                "8-495-123-32-32"));
        contacts.add(new Contact(2L,"Wildberries","Москва, ул. Ровная, д.71","http://wildberries.ru",
                "8-495-897-44-56"));
        contacts.add(new Contact(3L,"Улетная доставка","Москва, ул. Высокая, д.71","http://fly.ru",
                "8-495-999-54-80"));
        return contacts;
    }
}
