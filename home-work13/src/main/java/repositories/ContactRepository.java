package repositories;

import models.Contact;

import java.util.ArrayList;

public interface ContactRepository {
    ArrayList<Contact> loadAllContacts();
}
