package repositories;

import dto.SignUpForm;
import models.User;

import java.util.ArrayList;

public interface UserRepository {
    void save(SignUpForm form);
}
