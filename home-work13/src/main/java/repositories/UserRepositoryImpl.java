package repositories;

import dto.SignUpForm;
import models.User;
import java.util.ArrayList;

public class UserRepositoryImpl implements UserRepository {
    public static final ArrayList<User> users = new ArrayList<User>();
    private static Long countID = 0L;
    public void save(SignUpForm form) {
        User user = User.builder()
                .id(countID++)
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .age(form.getAge())
                .location(form.getLocation())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();
        System.out.println(user);
        users.add(user);
    }
}
