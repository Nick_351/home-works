package repositories;

import models.Product;
import java.util.ArrayList;

public class ProductRepositoryImpl implements ProductRepository {
    public static ArrayList<Product> products;
    public ArrayList<Product> loadAllProducts() {
        products = new ArrayList<Product>();
        products.add(new Product(1L,34764,"Кефир","Веселый молочник",50.60));
        products.add(new Product(2L,256490,"Сосиски","Весёленький мясник",350.0));
        products.add(new Product(3L,800300,"Сливочное масло","Домик в деревне",140.0));
        products.add(new Product(4L,559944,"Томатная паста","Синьор Помидор",125.0));
        return products;
    }
}
