package servlets;

import repositories.NewRepositoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/selectedNew")
public class SelectedNewServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("news", NewRepositoryImpl.news);
        req.setAttribute("id",req.getParameter("id"));
        req.getRequestDispatcher("jsp/selectedNew.jsp").forward(req,resp);
    }
}
