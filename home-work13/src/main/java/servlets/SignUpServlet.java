package servlets;

import dto.SignUpForm;
import repositories.UserRepositoryImpl;
import services.SignUpService;
import services.SignUpServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private SignUpService signUpService;
    @Override
    public void init() throws ServletException {
        signUpService = new SignUpServiceImpl(new UserRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signUp.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignUpForm form = SignUpForm.builder()
                .firstName(req.getParameter("firstName"))
                .lastName(req.getParameter("lastName"))
                .age(req.getParameter("age"))
                .location(req.getParameter("location"))
                .email(req.getParameter("email"))
                .password(req.getParameter("password"))
                .build();
        signUpService.save(form);

        req.setAttribute("firstName",req.getParameter("firstName"));
        req.setAttribute("lastName",form.getLastName());
        req.getRequestDispatcher("jsp/succesReg.jsp").forward(req,resp);
        resp.sendRedirect("jsp/succesReg.jsp");
    }
}
