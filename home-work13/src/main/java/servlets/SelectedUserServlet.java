package servlets;

import repositories.UserRepositoryImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/selectedUser")
public class SelectedUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", UserRepositoryImpl.users);
        req.setAttribute("id",req.getParameter("id"));
        req.getRequestDispatcher("jsp/selectedUser.jsp").forward(req,resp);
    }
}
