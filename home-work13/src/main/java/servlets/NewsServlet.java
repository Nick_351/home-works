package servlets;

import repositories.NewRepositoryImpl;
import services.LoadNewsService;
import services.LoadNewsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/news")
public class NewsServlet extends HttpServlet {
    private LoadNewsService loadNewsService;
    @Override
    public void init() throws ServletException {
        loadNewsService = new LoadNewsServiceImpl(new NewRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("news",loadNewsService.loadAllNews());
        req.getRequestDispatcher("jsp/news.jsp").forward(req,resp);
    }
}
