package servlets;

import repositories.ProductRepositoryImpl;
import services.LoadProductsService;
import services.LoadProductsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/products")
public class ProductsServlet extends HttpServlet {
    private LoadProductsService loadProductsService;
    @Override
    public void init() throws ServletException {
        loadProductsService = new LoadProductsServiceImpl(new ProductRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("products", loadProductsService.loadAllProducts());
        req.getRequestDispatcher("jsp/products.jsp").forward(req, resp);
    }
}
