package servlets;

import repositories.ProductRepositoryImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/selectedProduct")
public class SelectedProductServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("products", ProductRepositoryImpl.products);
        req.setAttribute("id",req.getParameter("id"));
        req.getRequestDispatcher("jsp/selectedProduct.jsp").forward(req,resp);
    }
}
