package servlets;

import repositories.ContactRepositoryImpl;
import services.LoadContactsService;
import services.LoadContactsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/contacts")
public class ContactsServlet extends HttpServlet {
    private LoadContactsService loadContactsService;

    @Override
    public void init() throws ServletException {
        loadContactsService = new LoadContactsServiceImpl(new ContactRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("contacts",loadContactsService.loadAllContacts());
        req.getRequestDispatcher("jsp/contacts.jsp").forward(req,resp);
    }
}
