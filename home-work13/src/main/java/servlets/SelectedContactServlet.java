package servlets;

import repositories.ContactRepositoryImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/selectedContact")
public class SelectedContactServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("contacts", ContactRepositoryImpl.contacts);
        req.setAttribute("id",req.getParameter("id"));
        req.getRequestDispatcher("jsp/selectedContact.jsp").forward(req,resp);
    }
}
