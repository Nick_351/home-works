package servlets;

import repositories.UserRepositoryImpl;
import services.LoadUsersService;
import services.LoadUsersServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    private LoadUsersService loadUsersService;

    @Override
    public void init() throws ServletException {
        loadUsersService = new LoadUsersServiceImpl(UserRepositoryImpl.users);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users",loadUsersService.loadAllUsers());
        req.getRequestDispatcher("jsp/users.jsp").forward(req,resp);
    }
}
