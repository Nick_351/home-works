package services;

import models.User;
import java.util.ArrayList;

public class LoadUsersServiceImpl implements LoadUsersService {
    private ArrayList<User> users;

    public LoadUsersServiceImpl(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<User> loadAllUsers() {
        return users;
    }
}
