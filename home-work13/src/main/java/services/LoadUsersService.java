package services;

import models.User;

import java.util.ArrayList;

public interface LoadUsersService {
    ArrayList<User> loadAllUsers();
}
