package services;

import dto.SignUpForm;
import repositories.UserRepository;

public class SignUpServiceImpl implements SignUpService {
    private UserRepository userRepository;
    public SignUpServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public void save(SignUpForm form) {
        userRepository.save(form);
    }
}
