package services;

import dto.SignUpForm;

public interface SignUpService {
    void save(SignUpForm form);
}
