package services;

import models.News;
import repositories.NewRepository;
import java.util.ArrayList;

public class LoadNewsServiceImpl implements LoadNewsService {
    private NewRepository newRepository;

    public LoadNewsServiceImpl(NewRepository newRepository) {
        this.newRepository = newRepository;
    }

    public ArrayList<News> loadAllNews() {
        return newRepository.loadAllNews();
    }
}
