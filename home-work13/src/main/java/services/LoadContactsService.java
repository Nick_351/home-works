package services;

import models.Contact;

import java.util.ArrayList;

public interface LoadContactsService {
    ArrayList<Contact> loadAllContacts();
}
