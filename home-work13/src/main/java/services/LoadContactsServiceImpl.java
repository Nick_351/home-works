package services;

import lombok.AllArgsConstructor;
import models.Contact;
import repositories.ContactRepository;
import java.util.ArrayList;
@AllArgsConstructor
public class LoadContactsServiceImpl implements LoadContactsService {
    private ContactRepository contactRepository;

    public ArrayList<Contact> loadAllContacts() {
        return contactRepository.loadAllContacts();
    }
}
