package services;

import models.Product;
import repositories.ProductRepository;
import java.util.ArrayList;

public class LoadProductsServiceImpl implements LoadProductsService {
    private ProductRepository productRepository;

    public LoadProductsServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    public ArrayList<Product> loadAllProducts() {
        return productRepository.loadAllProducts();
    }
}
