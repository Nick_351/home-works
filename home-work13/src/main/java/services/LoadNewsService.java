package services;

import models.News;
import java.util.ArrayList;

public interface LoadNewsService {
    ArrayList<News> loadAllNews();
}
