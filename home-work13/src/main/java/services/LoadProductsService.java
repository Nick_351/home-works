package services;

import models.Product;

import java.util.ArrayList;

public interface LoadProductsService {
    ArrayList<Product> loadAllProducts();
}
