package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contact {
    private Long id;
    private String nameShop;
    private String Address;
    private String site;
    private String telephoneNumber;
}
