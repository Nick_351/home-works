import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<String, Integer> myMap = new HashMap();
        System.out.print("Введите строку: ");
        String stroke = scan.nextLine();
        String[] myArray = stroke.split(" ");

        for(int i = 0; i < myArray.length; ++i) {
            if (!myMap.containsKey(myArray[i])) {
                myMap.put(myArray[i], 1);
            } else {
                myMap.put(myArray[i], (Integer)myMap.get(myArray[i]) + 1);
            }
        }
        for(Map.Entry<String, Integer> entry : myMap.entrySet()) {
            String key = (String)entry.getKey();
            Integer value = (Integer)entry.getValue();
            System.out.println(key + " - " + value);
        }
    }
}
