public class WebDesigner extends  Worker{
    public WebDesigner(String name, String lastName, String profession) {
        super(name, lastName, profession);;
    }
    @Override
    public void goToWork() {
        System.out.println("Меня зовут " + getName() + " " + getLastName() + ". Моя профессия - " + getProfession() + ". Я рисую веб-сайты и занимаюсь их вёрсткой.");
    }
    @Override
    public void goToVacation(int days) {
        System.out.println("В отпуск уходит " + getName() + " " + getLastName() + ". Его профессия - " + getProfession() + ". Он ушел в отпуск на " + days + " дней. Обычно " + getProfession() + " любит отдыхать, путешествуя по разным городам.");
    }
}

