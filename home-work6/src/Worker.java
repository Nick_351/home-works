import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Worker {
    private String name;
    private  String lastName;
    private String profession;

    public void goToWork() {
        System.out.println("Меня зовут " + getName() + " " + getLastName() + ". Моя профессия - " + getProfession() + ". Чем только мне не приходится заниматься).");
    }
    public void goToVacation(int days) {
        System.out.println("В отпуск уходит " + getName() + " " + getLastName() + ". Его профессия - " + getProfession() + ". Он ушел в отпуск на " + days + " дней. Обычно " + getProfession() + " отдыхает где попало и как попало.");
    }
}


