public class Programmer extends Worker{

    public Programmer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    @Override
    public void goToWork() {
        System.out.println("Меня зовут " + getName() + " " + getLastName() + ". Моя профессия - " + getProfession() + ". Я работаю над созданием десктопных приложений.");
    }
    @Override
    public void goToVacation(int days) {
        System.out.println("В отпуск уходит " + getName() + " " + getLastName() + ". Его профессия - " + getProfession() + ". Он ушел в отпуск на " + days + " дней. Обычно " + getProfession() + " любит отдыхать в теплых краях.");
    }
}

