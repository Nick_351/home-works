public class Plumber extends Worker {
    public Plumber(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    @Override
    public void goToWork() {
        System.out.println("Меня зовут " + getName() + " " + getLastName() + ". Моя профессия - " + getProfession() + ". Я занимаюсь установкой и ремонтом сантехники.");
    }
    @Override
    public void goToVacation(int days) {
        System.out.println("В отпуск уходит " + getName() + " " + getLastName() + ". Его профессия - " + getProfession() + ". Он ушел в отпуск на " + days + " дней. Обычно " + getProfession() + " любит отдыхать перед теликом с холодным пивком.");
    }
}

