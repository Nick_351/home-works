public class Main {

    public static void main(String[] args) {
        Worker worker = Worker.builder()
                .name("Иван")
                .lastName("Иванов")
                .profession("работающий человек")
                .build();
        worker.goToWork();
        worker.goToVacation(26);
        System.out.println();
        Worker engineer = Engineer.builder()
                .name("Стас")
                .lastName("Гайкин")
                .profession("инженер")
                .build();
        engineer.goToWork();
        engineer.goToVacation(18);
        System.out.println();
        Worker programmer = Programmer.builder()
                .name("Виктор")
                .lastName("Степанов")
                .profession("программист")
                .build();
        programmer.goToWork();
        programmer.goToVacation(11);
        System.out.println();
        Worker plumber = Plumber.builder()
                .name("Эдуард")
                .lastName("Трубкин")
                .profession("сантехник")
                .build();
        plumber.goToWork();
        plumber.goToVacation(15);
        System.out.println();
        Worker webDesigner = WebDesigner.builder()
                .name("Александр")
                .lastName("Рисовалкин")
                .profession("веб-дизайнер")
                .build();
        webDesigner.goToWork();
        webDesigner.goToVacation(30);
    }
}

