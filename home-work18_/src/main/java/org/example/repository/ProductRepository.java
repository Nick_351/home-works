package org.example.repository;
import org.example.model.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository {

    void save(Product product);

    List<Product> getAllProducts();

    Product getByID(Long id);
    Product deleteProduct(Long id);
    Product editProduct(Product product, Long id);
}
