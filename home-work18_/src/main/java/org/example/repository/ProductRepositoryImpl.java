package org.example.repository;

import org.example.model.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {


    private DataBaseHiber dataBaseHiber;

    @Autowired
    public ProductRepositoryImpl(DataBaseHiber dataBaseHiber) {
        this.dataBaseHiber = dataBaseHiber;
    }

    @Override
    public void save(Product product) {
        try(Session session = dataBaseHiber.getSession()) {
            session.save(product);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> getAllProducts() {
        try(Session session = dataBaseHiber.getSession()) {
            NativeQuery<Product> query = session.createNativeQuery("select * from product", Product.class);
            return query.getResultList();
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product getByID(Long id) {
        try(Session session = dataBaseHiber.getSession()){
            NativeQuery<Product> query = session.createNativeQuery("select * from product where id = :id",
                    Product.class);
            query.setParameter("id", id);
            List<Product> products = query.getResultList();
            Product product = products.stream().findFirst().get();
            return product;
        } catch (RuntimeException e){
            throw  new RuntimeException(e);
        }
    }
    @Override
    public Product deleteProduct(Long id) {
        try(Session session = dataBaseHiber.getSession()){
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery("delete from product where id = :id", Product.class)
                .setParameter("id", id)
                .executeUpdate();
            transaction.commit();
        return null;
        } catch (RuntimeException e){
            throw  new RuntimeException(e);
        }
    }

    @Override
    public Product editProduct(Product product, Long id) {
        try(Session session = dataBaseHiber.getSession()){
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery("update product set name = :name, brand = :brand, code = :code, " +
                            "price = :price where id = :id", Product.class)
                    .setParameter("id", id)
                    .setParameter("name", product.getName())
                    .setParameter("brand", product.getBrand())
                    .setParameter("code", product.getCode())
                    .setParameter("price", product.getPrice())
                    .executeUpdate();
            transaction.commit();
            return null;
        } catch (RuntimeException e){
            throw  new RuntimeException(e);
        }
    }
}
