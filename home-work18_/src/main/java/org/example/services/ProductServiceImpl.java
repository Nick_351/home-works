package org.example.services;

import org.example.dto.ProductDto;
import org.example.model.Product;
import org.example.repository.ProductRepository;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void saveProduct(ProductDto productDto) {
        Product product = Product.builder()
                .name(productDto.getName())
                .brand(productDto.getBrand())
                .price(productDto.getPrice())
                .code(productDto.getCode())
                .build();

        productRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public Product getByID(Long id) {
        Optional<Product> product = Optional.ofNullable(productRepository.getByID(id));
        return product.orElse(Product.builder().build());
    }

    @Override
    public void deleteByID(Long id) {
        productRepository.deleteProduct(id);
    }

    @Override
    public void editByID(Long id, ProductDto productDto) {
        Product product = Product.builder()
                .name(productDto.getName())
                .brand(productDto.getBrand())
                .price(productDto.getPrice())
                .code(productDto.getCode())
                .build();
        productRepository.editProduct(product, id);
    }
}
