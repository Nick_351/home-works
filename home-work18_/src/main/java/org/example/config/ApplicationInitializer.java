package org.example.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ApplicationInitializer implements WebApplicationInitializer {
@Override
public void onStartup(ServletContext servletContext) throws ServletException {
    //Класс для настройки web приложения
    AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
    //Передаем настройку нашего приложения
    webApplicationContext.register(ApplicationConfig.class);
    //Нужно связать жизненный цикл ServletContext c нашим приложением
    ContextLoaderListener contextLoaderListener = new ContextLoaderListener(webApplicationContext);
    //Связываем работу контейнера сервлетов с нашим приложением
    servletContext.addListener(contextLoaderListener);

    ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher", new DispatcherServlet(webApplicationContext));
    dispatcherServlet.setLoadOnStartup(1);
    dispatcherServlet.addMapping("/");
}

}
