package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.dto.ProductDto;
import org.example.services.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/registrationProduct")
@RequiredArgsConstructor
public class ProductRegController {

    private final ProductService productService;
    @RequestMapping(method = RequestMethod.GET)
    public String getRegistrationProductPage(){
        return "registrationProduct";
    }
//    @PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String registrationProduct(ProductDto productDto){
        productService.saveProduct(productDto);
        return "redirect:/products";
    }

}
