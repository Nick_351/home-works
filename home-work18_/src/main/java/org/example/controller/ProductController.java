package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.dto.ProductDto;
import org.example.services.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public String getListProducts(Model model){
        model.addAttribute("products", productService.getAllProducts());
        return "products";
    }
    @RequestMapping(value = "/{product_id}", method = RequestMethod.GET)
    public String getProduct(Model model, @PathVariable(value = "product_id") Long id){
        model.addAttribute("product", productService.getByID(id));
        return "product";
    }
    @RequestMapping(value = "delete/{product_id}", method = RequestMethod.POST)
    public String deleteProduct(Model model, @PathVariable(value = "product_id") Long id){
        productService.deleteByID(id);
        return "redirect:/products";
    }
    @RequestMapping(value = "edit/{product_id}", method = RequestMethod.POST)
    public String editProduct(Model model, @PathVariable(value = "product_id") Long id){
        model.addAttribute("editProduct", productService.getByID(id));
        return "editProduct";
    }
    @RequestMapping(value = "edit/edition/{product_id}", method = RequestMethod.POST)
    public String editionProduct(Model model, @PathVariable(value = "product_id") Long id, ProductDto productDto){
        productService.editByID(id, productDto);
        return "redirect:/products";
    }
}
