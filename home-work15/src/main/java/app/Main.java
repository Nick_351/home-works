package app;

import models.Account;
import services.*;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {

        System.out.println("Выберите режим работы:");
        System.out.println("1 - Создать новый аккаунт");
        System.out.println("2 - Получить данные по всем аккаунтам");
        System.out.println("3 - Получить данные аккаунта по id");
        System.out.println("4 - Редактировать аккаунт");
        System.out.println("5 - Удалить аккаунт");
        System.out.println("----------------------------------------");
        Scanner scanner = new Scanner(System.in);
        int result = scanner.nextInt();
        if(result == 1){
            SaveAccounts saveAccounts = new SaveAccountsImpl(new CreateAccountsImpl().createAccount());
            saveAccounts.save();
            System.out.println("Аккаунт успешно сохранен!");
        }
        if(result == 2){
            TakeAccounts takeAccounts = new TakeAccountsImpl();
            takeAccounts.takeAllAccounts();
        }
        if (result == 3){
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Введите id аккаунта: ");
            Long resultID = scanner2.nextLong();
            TakeAccounts takeAccounts = new TakeAccountsImpl();
            Account account = takeAccounts.takeAccountByIDAccount(resultID);
            if(account == null){
                System.out.println("Аккаунта с таким id нет в базе!");
            }
        }
        if(result == 4){
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Введите id аккаунта для редактирования: ");
            Long resultID = scanner2.nextLong();
            TakeAccounts takeAccounts = new TakeAccountsImpl();
            Account account = takeAccounts.takeAccountByIDAccount(resultID); //вывод аккаунта, который будет редактироваться
            if(account != null) {
                EditAccounts editAccount = new EditAccounstImpl(new CreateAccountsImpl().createAccount());
                editAccount.editAccount(resultID);
                System.out.println("Аккаунт успешно отредактирован!");
            }
            else {
                System.out.println("Такого аккаунта нет в базе!");
            }
        }
        if(result  == 5){
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Введите id аккаунта для удаления: ");
            Long resultID = scanner2.nextLong();
            TakeAccounts takeAccounts = new TakeAccountsImpl();
            Account account = takeAccounts.takeAccountByIDAccount(resultID);
            if(account != null){
                System.out.println("Вы действительно хотите удалить аккаунт? ДА - 1; НЕТ - 2");
                int choise = scanner2.nextInt();
                if(choise == 1){
                    DeleteAccounts deleteAccounts = new DeleteAccountsImpl();
                    deleteAccounts.deleteAccount(resultID);
                    System.out.println("Аккаунт успешно удален!");
                }
                else{
                    System.out.println("Программа завершила работу");
                }
            }
            else{
                System.out.println("Такого аккаунта нет в базе!");
            }
        }
    }
}
