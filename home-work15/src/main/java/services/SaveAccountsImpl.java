package services;

import models.Account;
import requests.DBqueries;
import java.sql.*;
import java.time.LocalDate;

public class SaveAccountsImpl implements SaveAccounts {
    private Account account;

    public SaveAccountsImpl(Account account) {
        this.account = account;
    }

    @Override
    public void save() {
        String request = DBqueries.SAVE_ACCOUNT.toString();
        try {
            Connect connect = new Connect();
            PreparedStatement preparedStatement = connect.connectDB().prepareStatement(request);
            preparedStatement.setString(1, account.getFirstName());
            preparedStatement.setString(2, account.getLastName());
            preparedStatement.setString(3, account.getEmail());
            preparedStatement.setDate(4, Date.valueOf(LocalDate.now()));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
