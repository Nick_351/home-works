package services;

import models.Account;

import java.util.Scanner;

public class CreateAccountsImpl implements CreateAccounts {
    @Override
    public Account createAccount() {
        Account account = new Account();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите Ваше имя: ");
        account.setFirstName(scanner.nextLine());
        System.out.println("Введите Вашу фамилию: ");
        account.setLastName(scanner.nextLine());
        System.out.println("Введите Ваш e-mail: ");
        account.setEmail(scanner.nextLine());
        return account;
    }
}
