package services;

import models.Account;

import java.sql.SQLException;

public interface SaveAccounts {
    void save () throws SQLException;
}
