package services;

import models.Account;
import requests.DBqueries;
import java.sql.*;
import java.time.LocalDate;

public class TakeAccountsImpl implements TakeAccounts {
    @Override
    public void takeAllAccounts()  {
        String request = DBqueries.TAKE_ALL_ACCOUNTS.toString();
        try{
            Connect connect = new Connect();
            PreparedStatement preparedStatement = connect.connectDB().prepareStatement(request);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()){
                Long id = result.getLong("id");
                String firstName = result.getString("first_name");
                String lastName = result.getString("last_name");
                String email = result.getString("email");
                LocalDate createdAt = result.getDate("create_dat").toLocalDate();
                Account account = Account.builder()
                        .id(id)
                        .firstName(firstName)
                        .lastName(lastName)
                        .email(email)
                        .createdAt(createdAt)
                        .build();
                System.out.println("id - " + account.getId() + " " + account.getFirstName() + " " + account.getLastName() +
                        ", e-mail: " + account.getEmail() + ", дата регистрации - " + account.getCreatedAt());
        }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Account takeAccountByIDAccount(Long id) {
        String request = DBqueries.TAKE_ACCOUNT_BY_ID_RETURN_ACCOUNT.toString();
        try{
            Connect connect = new Connect();
            PreparedStatement preparedStatement = connect.connectDB().prepareStatement(request);
            preparedStatement.setLong(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if(result.next()){
                String firstName = result.getString("first_name");
                String lastName = result.getString("last_name");
                String email = result.getString("email");
                LocalDate createdAt = result.getDate("create_dat").toLocalDate();
                Account account = Account.builder()
                        .id(id)
                        .firstName(firstName)
                        .lastName(lastName)
                        .email(email)
                        .createdAt(createdAt)
                        .build();
                System.out.println("id - " + account.getId() + " " + account.getFirstName() + " " + account.getLastName() +
                        ", e-mail: " + account.getEmail() + ", дата регистрации - " + account.getCreatedAt());
                return account;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
