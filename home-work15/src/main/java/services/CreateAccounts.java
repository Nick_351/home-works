package services;

import models.Account;

public interface CreateAccounts {
    Account createAccount();
}
