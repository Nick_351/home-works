package services;

public interface DeleteAccounts {
    void deleteAccount(Long id);
}
