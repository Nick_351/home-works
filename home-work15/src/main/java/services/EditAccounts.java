package services;

import java.sql.SQLException;

public interface EditAccounts {
    void editAccount(Long id) throws SQLException;
}
