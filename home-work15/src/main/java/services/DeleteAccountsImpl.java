package services;

import requests.DBqueries;
import java.sql.*;
import java.time.LocalDate;

public class DeleteAccountsImpl implements DeleteAccounts {
    public void deleteAccount(Long id) {
        String request = DBqueries.DELETE_ACCOUNT.toString();
        try{
            Connect connect = new Connect();
            PreparedStatement preparedStatement = connect.connectDB().prepareStatement(request);
            preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
