package services;

import models.Account;
import requests.DBqueries;
import java.sql.*;

public class EditAccounstImpl implements EditAccounts {
    private Account account;

    public EditAccounstImpl(Account account) {
        this.account = account;
    }

    @Override
    public void editAccount(Long id) {
        String request = DBqueries.EDIT_ACCOUNT.toString();
        try{
            Connect connect = new Connect();
            PreparedStatement preparedStatement = connect.connectDB().prepareStatement(request);
            preparedStatement.setString(1,account.getFirstName());
            preparedStatement.setString(2,account.getLastName());
            preparedStatement.setString(3,account.getEmail());
            preparedStatement.setLong(4, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
