package services;

import models.Account;

import java.sql.SQLException;

public interface TakeAccounts {
    void takeAllAccounts() throws SQLException;
//    void takeAccountByID(Long id) throws SQLException;
    Account takeAccountByIDAccount(Long id) throws SQLException;
}
