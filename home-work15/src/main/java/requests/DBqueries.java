package requests;

public enum DBqueries {
    TAKE_ALL_ACCOUNTS("SELECT * FROM account WHERE delete_dat IS NULL"),
    TAKE_ACCOUNT_BY_ID_RETURN_ACCOUNT("SELECT * FROM account WHERE id = ? AND delete_dat IS NULL"),
    SAVE_ACCOUNT("INSERT INTO account (first_name, last_name, email, create_dat) VALUES (?, ?, ?, ?)"),
    EDIT_ACCOUNT("UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE id = ?"),
    DELETE_ACCOUNT("UPDATE account SET delete_dat = ? WHERE id = ?");

    private String title;

    DBqueries(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
