import java.util.Arrays;
import java.util.Scanner;

public class ArraySort {
    public static void ArrayMove() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int volume = scan.nextInt();
        int[] array = new int[volume];
        int support1 = 0;
        int support2 = 0;
        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите элемент массива номер " + i + " :");
            array[i] = scan.nextInt();
        }

        for (int j=0; j<array.length-1; j++){
            for (int i = 0; i < array.length - 1; i++) {

                if (array[i] == 0) {
                    support1 = array[i]; // значение проверяемого элемента
                    support2 = array[i + 1]; // значение соседнего справа элемента
                    array[i] = support2;
                    array[i + 1] = support1;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        ArrayMove();
    }
}
