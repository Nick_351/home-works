package org.example.mvc.repository;

import org.example.mvc.model.Car;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository {
    void save(Car car);
    List<Car> getAllCars();
}
