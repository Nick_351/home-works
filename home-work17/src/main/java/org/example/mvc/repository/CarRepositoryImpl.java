package org.example.mvc.repository;

import org.example.mvc.model.Car;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {
    private final DataBaseHiber dataBaseHiber;

    @Autowired
    public CarRepositoryImpl(DataBaseHiber dataBaseHiber) {
        this.dataBaseHiber = dataBaseHiber;
    }

    @Override
    public void save(Car car) {
        try(Session session = dataBaseHiber.getSession()){
            session.save(car);
        }
        catch (RuntimeException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Car> getAllCars() {
        try(Session session = dataBaseHiber.getSession()){
            NativeQuery<Car> query = session.createNativeQuery("select * from car", Car.class);
            return query.getResultList();
        }
        catch (RuntimeException e){
            throw new RuntimeException(e);
        }
    }
}
