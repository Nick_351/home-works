package org.example.mvc.controller;

import lombok.RequiredArgsConstructor;
import org.example.mvc.dto.CarDto;
import org.example.mvc.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class RegistrationCarController implements Controller {
    private final CarService carService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        if(request.getMethod().equalsIgnoreCase("post")){
            CarDto carDto = CarDto.builder()
                    .brand(request.getParameter("brand"))
                    .model(request.getParameter("model"))
                    .color(request.getParameter("color"))
                    .yearOfProduction(request.getParameter("yearOfProduction"))
                    .build();
            carService.createCar(carDto);
            modelAndView.setViewName("successfulRegistration");
        } else {
            modelAndView.setViewName("registrationCar");
        }
        return  modelAndView;
    }
}
