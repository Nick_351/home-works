package org.example.mvc.service;

import org.example.mvc.dto.CarDto;
import org.example.mvc.model.Car;
import org.example.mvc.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {
    private CarRepository carRepository;
    @Autowired
    public CarServiceImpl(CarRepository carRepository){
        this.carRepository = carRepository;
    }
    @Override
    public void createCar(CarDto carDto) {
        Car car = Car.builder()
                .brand(carDto.getBrand())
                .model(carDto.getModel())
                .color(carDto.getColor())
                .yearOfProduction(carDto.getYearOfProduction())
                .build();
        carRepository.save(car);
    }

    @Override
    public List<Car> getAllCars() {
        return carRepository.getAllCars();
    }
}
