package org.example.mvc.service;

import org.example.mvc.dto.CarDto;
import org.example.mvc.model.Car;

import java.util.List;

public interface CarService {
    void createCar(CarDto carDto);
    List<Car> getAllCars();
}
