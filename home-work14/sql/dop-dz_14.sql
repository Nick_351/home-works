CREATE TABLE driver
(
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(50)
)

CREATE TABLE engine
(
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(50),
	power INTEGER,
	capacity DECIMAL
)

CREATE TABLE car(
	id BIGSERIAL PRIMARY KEY,
	brand VARCHAR(50),
	model VARCHAR(50),
	color VARCHAR(30),
	driver_id INTEGER REFERENCES driver,
	engine_id INTEGER REFERENCES engine
)
INSERT INTO driver (name) VALUES ('Виктор');
INSERT INTO driver (name) VALUES ('Сергей');
INSERT INTO driver (name) VALUES ('Рюрик');
INSERT INTO driver (name) VALUES ('Александр');
INSERT INTO driver (name) VALUES ('Трувор');

INSERT INTO engine (name, power, capacity) VALUES ('S54', 315, 3.2);
INSERT INTO engine (name, power, capacity) VALUES ('2KD-FTV', 144, 2.5);
INSERT INTO engine (name, power, capacity) VALUES ('OM 639', 80, 1.5);
INSERT INTO engine (name, power, capacity) VALUES ('ЗМЗ-405', 152, 2.5);
INSERT INTO engine (name, power, capacity) VALUES ('HR16DE/H4M', 108, 1.6);

INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('BMW', 'X3', 'black', 1, 4);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Audi', 'A4', 'red', 2, 5);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('KIA', 'Ceed', 'gray', 3, 3);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Opel', 'Vectra', 'white', 4, 2);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('BMW', 'X6', 'yellow', 5, 1);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Audi', 'TT', 'blue', 1, 3);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Opel', 'Zafira', 'black', 2, 4);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Opel', 'Mokka', 'brown', 3, 1);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Ford', 'Mondeo', 'purple', 4, 4);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Ford', 'Focus', 'black', 5, 2);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Nissan', 'Caravan', 'pink', 1, 2);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Nissan', 'Primera', 'yellow', 2, 5);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('УАЗ', 'Буханка', 'black', 3, 5);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('Nissan', 'Note', 'blue', 4, 3);
INSERT INTO car (brand, model, color, driver_id, engine_id) VALUES ('ГАЗ', '31029 Волга', 'red', 4, 3);


SELECT * FROM car JOIN driver ON car.driver_id = driver.id JOIN engine ON car.engine_id = engine.id;
SELECT * FROM car JOIN driver ON car.driver_id = driver.id JOIN engine ON car.engine_id = engine.id WHERE engine.capacity > 2.0;
SELECT * FROM car JOIN driver ON car.driver_id = driver.id JOIN engine ON car.engine_id = engine.id WHERE engine.power > 200;
SELECT * FROM car JOIN driver ON car.driver_id = driver.id JOIN engine ON car.engine_id = engine.id WHERE car.color = 'black';

