CREATE TABLE country
(
id BIGSERIAL PRIMARY KEY,
name VARCHAR(50),
population INTEGER
);

INSERT INTO country VALUES(1,'Индия',1416394000);
INSERT INTO country VALUES(2,'Индонезия',290000000);
INSERT INTO country VALUES(3,'Бразилия',220174120);
INSERT INTO country VALUES(4,'Россия',147182123);
INSERT INTO country VALUES(5,'Япония',127470000);
INSERT INTO country VALUES(6,'Эфиопия',125656596);
INSERT INTO country VALUES(7,'Вьетнам',100010000);
INSERT INTO country VALUES(8,'Германия',85000000);
INSERT INTO country VALUES(9,'Польша',37749000);
INSERT INTO country VALUES(10,'Чехия',10524167);
INSERT INTO country VALUES(11,'Гондурас',9158345);
INSERT INTO country VALUES(12,'Израиль',9136000);

SELECT name FROM country ORDER BY population DESC LIMIT 3;
SELECT name FROM country ORDER BY population ASC LIMIT 3;

SELECT SUM (population) / COUNT (population) FROM country; (SELECT AVG (population) FROM country;)

SELECT name, population FROM country GROUP BY population, name HAVING (population > (SELECT AVG(population) FROM country));
SELECT name, population FROM country GROUP BY population, name HAVING (population < (SELECT AVG(population) FROM country));

SELECT name From country WHERE population = (SELECT MAX(population) FROM country);
SELECT name From country WHERE population = (SELECT MIN(population) FROM country);
