import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashSet<Number> baseOfCarNumber = new HashSet<Number>();
        Scanner scan = new Scanner(System.in);

        for(;;){
            System.out.println("Введите Ваш регион: ");
            String region = scan.nextLine();
            System.out.println("Введите номер, который Вы хотите: ");
            String value = scan.nextLine();
            Number number = Number.builder()
                    .region(region)
                    .value(value)
                    .build();
            if(baseOfCarNumber.add(number) == false){
                System.out.println("Такой номер существует, введите другой!");
            }
            else {
                baseOfCarNumber.add(number);
            }
        }
    }
}
