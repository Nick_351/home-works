import lombok.*;
@Builder
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class Number {
    private String value;
    private String region;
}
