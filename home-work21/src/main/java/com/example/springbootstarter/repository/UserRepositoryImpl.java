package com.example.springbootstarter.repository;

import com.example.springbootstarter.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

//@Component
//public class UserRepositoryImpl implements UserRepository {

//	public static final List<User> USERS = new ArrayList<>(Collections.singletonList(User.builder()
//			.id(182425314L)
//			.firstName("Oleg")
//			.lastName("Igonin")
//			.role(User.Role.ADMIN)
//			.build()));
//	@Override
//	public List<User> findAll() {
//		return USERS;
//	}
//
//	@Override
//	public Optional<User> findById(Long id) {
//		return USERS.stream().filter(user -> user.getId().equals(id)).findFirst();
//	}
//
//	@Override
//	public void save(User user) {
//		USERS.add(user);
//	}
//}
