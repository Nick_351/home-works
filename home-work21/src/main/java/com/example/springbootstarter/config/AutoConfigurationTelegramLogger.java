package com.example.springbootstarter.config;

import com.example.springbootstarter.repository.UserRepository;
import com.example.springbootstarter.service.TelegramBotService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableConfigurationProperties(TelegramLoggerProperties.class)
@ComponentScan("com.example.springbootstarter")
@EnableJpaRepositories("com.example.springbootstarter.repository")
@EntityScan("com.example.springbootstarter.model")
public class AutoConfigurationTelegramLogger {

	@Bean
	public TelegramBotService telegramBotService(TelegramLoggerProperties properties,
												 Environment environment, UserRepository userRepository) {
		return new TelegramBotService(properties, userRepository, environment);
	}
}
