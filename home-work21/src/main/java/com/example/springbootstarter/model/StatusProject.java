package com.example.springbootstarter.model;

public enum StatusProject {
	LAUNCHED, STOPPED
}
