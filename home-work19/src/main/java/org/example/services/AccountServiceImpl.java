package org.example.services;

import org.example.model.Account;
import org.example.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> searchAllBySubscriptionsContains() {
        return accountRepository.searchAllBySubscriptionsContains();
    }
    public List<Account> searchAllByBalance(Long balance){
        return accountRepository.searchAllByBalance(balance);
    }

    @Override
    public List<Account> searchAllBySubscriptions(int number) {
        return accountRepository.searchAllBySubscriptions(number);
    }

    @Override
    public List<Account> searchAccountBySubscriptions(String name) {
        return accountRepository.searchAccountBySubscriptions(name);
    }

    @Override
    public List<Account> searchAccountBySubscriptionsSum(Long sum) {
        return accountRepository.searchAccountBySubscriptionsSum(sum);
    }

    @Override
    public List<Account> searchAccountBySubscriptionsEndingInData(LocalDate date) {
        return accountRepository.searchAccountBySubscriptionsEndingInData(date);
    }

    @Override
    public List<Account> searchAccountBySubscriptionsIsWorkingInData(LocalDate date) {
        return accountRepository.searchAccountBySubscriptionsIsWorkingInData(date);
    }
}
