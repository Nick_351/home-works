package org.example.services;

import org.example.dto.AccountDto;
import org.example.model.Account;
import org.example.model.Subscription;
import org.example.repository.AccountRepositoryOldSolution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountServiceOldSolutionImpl implements AccountServiceOldSolution {

    private final AccountRepositoryOldSolution accountRepository;

    @Autowired
    public AccountServiceOldSolutionImpl(AccountRepositoryOldSolution accountRepositoryOldSolution) {
        this.accountRepository = accountRepositoryOldSolution;
    }

    @Override
    public void saveAccount(AccountDto accountDto) {
        Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .patronymic(accountDto.getPatronymic())
                .email(accountDto.getEmail())
                .balance(accountDto.getBalance())
                .subscriptions(accountDto.getSubscriptions())
                .build();
        accountRepository.saveAccount(account);
    }

    @Override
    public void saveSubscription(Subscription subscription) {
        accountRepository.saveSubscription(subscription);
    }
}
