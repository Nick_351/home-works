package org.example.services;
import java.time.LocalDate;
import java.util.List;
import org.example.model.Account;


public interface AccountService {
    List<Account> searchAllBySubscriptionsContains();
    List<Account> searchAllByBalance(Long balance);
    List<Account> searchAllBySubscriptions(int number);
    List<Account> searchAccountBySubscriptions(String name);
    List<Account> searchAccountBySubscriptionsSum(Long sum);
    List<Account> searchAccountBySubscriptionsEndingInData(LocalDate date);
    List<Account> searchAccountBySubscriptionsIsWorkingInData(LocalDate date);
}
