package org.example.services;

import org.example.dto.AccountDto;
import org.example.model.Subscription;

public interface AccountServiceOldSolution {

    void saveAccount(AccountDto productDto);
    void saveSubscription(Subscription subscription);

}
