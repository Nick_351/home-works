package org.example.repository;

import org.example.model.Account;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountRepository extends JpaRepository<Account, Long> {

    //Поиск всех аккаунтов, у которых нет подписок.
    @Query("select acc from Account acc where acc.subscriptions.size = 0")
    List<Account> searchAllBySubscriptionsContains();

    //Поиск всех аккаунтов, у которых баланс больше определенной суммы.
    @Query("select acc from Account acc where acc.balance > :balance")
    List<Account> searchAllByBalance(@Param("balance") Long balance);

    //Поиск всех аккаунтов, у которых подписок больше определенного количества.
    @Query("select acc from Account acc where acc.subscriptions.size > :number")
    List<Account> searchAllBySubscriptions(@Param("number") int number);

    //Поиск всех аккаунтов, у которых есть определенная подписка.
    @Query("select acc from Account acc join acc.subscriptions sub where sub.name = :name")
    List<Account> searchAccountBySubscriptions(@Param("name") String name);

    //Поиск всех аккаунтов, у которых подписка стоит больше определенной суммы.
    @Query("select acc from Account acc join acc.subscriptions sub where sub.cost > :sum")
    List<Account> searchAccountBySubscriptionsSum(@Param("sum") Long sum);
    //ДОП. ДЗ
    //Поиск всех аккаунтов, у которых подписка заканчивается в определенную дату.
    @Query("select acc from Account acc join acc.subscriptions sub where sub.before = :date")
    List<Account> searchAccountBySubscriptionsEndingInData(@Param("date") LocalDate date);

    //Поиск всех аккаунтов, у которых подписка еще действует в определенную дату.
    @Query("select distinct acc from Account acc join acc.subscriptions sub where sub.before > :date")
    List<Account> searchAccountBySubscriptionsIsWorkingInData(@Param("date") LocalDate date);
}
