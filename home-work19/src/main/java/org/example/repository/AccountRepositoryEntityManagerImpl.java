package org.example.repository;

import org.example.model.Account;
import org.example.model.Subscription;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class AccountRepositoryEntityManagerImpl implements AccountRepositoryOldSolution {
    @PersistenceContext
    private EntityManager entityManager;
    @Transactional
    @Override
    public void saveAccount(Account account) {
        entityManager.persist(account);
    }

    @Transactional
    @Override
    public void saveSubscription(Subscription subscription) {
        entityManager.persist(subscription);
    }

}
