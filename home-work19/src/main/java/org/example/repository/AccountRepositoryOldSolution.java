package org.example.repository;
import org.example.model.Account;
import org.example.model.Subscription;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepositoryOldSolution {

    void saveAccount(Account account);
    void saveSubscription(Subscription subscription);

}
