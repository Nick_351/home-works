package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.Subscription;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private Long balance;
    private List<Subscription> subscriptions;
}
