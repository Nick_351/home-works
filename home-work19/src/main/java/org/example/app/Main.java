package org.example.app;

import org.example.config.ApplicationConfig;
import org.example.dto.AccountDto;
import org.example.model.Account;
import org.example.model.Subscription;
import org.example.services.AccountService;
import org.example.services.AccountServiceOldSolution;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

//        Subscription subscription1 = Subscription.builder()
//                .name("Яндекс+")
//                .description("Какие-то плюхи от Яндекса")
//                .cost(290L)
//                .before(LocalDate.of(2023, 12, 31))
//                .build();

//        Subscription subscription2 = Subscription.builder()
//                .name("Google Play Музыка")
//                .description("Множество песен, подборки по настроению...")
//                .cost(390L)
//                .before(LocalDate.of(2023, 11, 25))
//                .build();
//
//        Subscription subscription3 = Subscription.builder()
//                .name("Амедиатека")
//                .description("Библиотека HBO, лучшие сериалы FOX, Showtime, Starz, BBC...")
//                .cost(599L)
//                .before(LocalDate.of(2023, 11, 25))
//                .build();
//
//        Subscription subscription4 = Subscription.builder()
//                .name("Netflix")
//                .description("Библиотека культовых фильмов и сериалов...")
//                .cost(569L)
//                .before(LocalDate.of(2023, 12, 31))
//                .build();
//
//        Subscription subscription5 = Subscription.builder()
//                .name("YouTube Premium")
//                .description("Просмотр видео на YouTube без рекламы")
//                .cost(569L)
//                .before(LocalDate.of(2023, 12, 20))
//                .build();
//
//        Subscription subscription6 = Subscription.builder()
//                .name("SkyEng")
//                .description("Онлайн‑уроки...")
//                .cost(300L)
//                .before(LocalDate.of(2023, 11, 22))
//                .build();
//
//        Subscription subscription7 = Subscription.builder()
//                .name("AudioBooks")
//                .description("Сервис для прослушивания аудиокниг")
//                .cost(420L)
//                .before(LocalDate.of(2023, 12, 25))
//                .build();
//
//        AccountDto accountDto = AccountDto.builder()
//                .firstName("Сергей")
//                .lastName("Котов")
//                .patronymic("Владимирович")
//                .email("kotS@mail.ru")
//                .balance(5700L)
//                .subscriptions(Arrays.asList(subscription1, subscription2))
//                .build();
//
//        AccountDto accountDto2 = AccountDto.builder()
//                .firstName("Артур")
//                .lastName("Пирожков")
//                .patronymic("Альбертович")
//                .email("ArturKA@mail.ru")
//                .balance(5700L)
//                .subscriptions(Arrays.asList(subscription3, subscription5, subscription7))
//                .build();
//
//        AccountDto accountDto3 = AccountDto.builder()
//                .firstName("Антон")
//                .lastName("Свиридов")
//                .patronymic("Сергеевич")
//                .email("Toha@mail.ru")
//                .balance(300L)
//                .subscriptions(Arrays.asList(subscription1, subscription2))
//                .build();

//        AccountDto accountDto4 = AccountDto.builder()
//                .firstName("Василий")
//                .lastName("Кондратьев")
//                .patronymic("Олегович")
//                .email("Basiliy@mail.ru")
//                .balance(2500L)
//                .subscriptions(Arrays.asList(subscription4, subscription6, subscription1, subscription3))
//                .build();
//
//        AccountDto accountDto5 = AccountDto.builder()
//                .firstName("Антон")
//                .lastName("Свиридов")
//                .patronymic("Сергеевич")
//                .email("Toha@mail.ru")
//                .balance(1100L)
//                .subscriptions(Arrays.asList(subscription1, subscription2))
//                .build();
//
//        AccountDto accountDto6 = AccountDto.builder()
//                .firstName("Платон")
//                .lastName("Иванов")
//                .patronymic("Викторович")
//                .email("Platoha@mail.ru")
//                .balance(200L)
//                .subscriptions(Arrays.asList())
//                .build();
//
//        AccountDto accountDto7 = AccountDto.builder()
//                .firstName("Гарри")
//                .lastName("Поттер")
//                .patronymic("Джеймс")
//                .email("Garik@mail.ru")
//                .balance(1400L)
//                .subscriptions(Arrays.asList(subscription3, subscription4))
//                .build();

//        AccountServiceOldSolution accountService = (AccountServiceOldSolution) applicationContext.getBean("accountServiceOldSolutionImpl");
//        accountService.saveSubscription(subscription1);
//        accountService.saveSubscription(subscription2);
//        accountService.saveSubscription(subscription3);
//        accountService.saveSubscription(subscription4);
//        accountService.saveSubscription(subscription5);
//        accountService.saveSubscription(subscription6);
//        accountService.saveSubscription(subscription7);
//        accountService.saveAccount(accountDto);
//        accountService.saveAccount(accountDto2);
//        accountService.saveAccount(accountDto3);
//        accountService.saveAccount(accountDto4);
//        accountService.saveAccount(accountDto5);
//        accountService.saveAccount(accountDto6);
//        accountService.saveAccount(accountDto7);
        AccountService accountService = (AccountService) applicationContext.getBean("accountServiceImpl");

        List<Account> listWithoutSubscriptions = accountService.searchAllBySubscriptionsContains();
        System.out.println(listWithoutSubscriptions.toString());

        List<Account> listWithBalance = accountService.searchAllByBalance(1000L);
        System.out.println(listWithBalance.toString());

        List<Account> listWithSubscriptions = accountService.searchAllBySubscriptions(2);
        System.out.println(listWithSubscriptions.toString());

        List<Account> listWithSelectSubscription = accountService.searchAccountBySubscriptions("Амедиатека");
        System.out.println(listWithSelectSubscription.toString());

        List<Account> listWithSelectSubscriptionSum = accountService.searchAccountBySubscriptionsSum(570L);
        System.out.println(listWithSelectSubscriptionSum.toString());

        List<Account> listWithSubscriptionsEndingInDate = accountService.searchAccountBySubscriptionsEndingInData(LocalDate.of(2023, 11, 22));
        System.out.println(listWithSubscriptionsEndingInDate.toString());

        List<Account> listWithSubscriptionsIsWorkingInDate = accountService.searchAccountBySubscriptionsIsWorkingInData(LocalDate.of(2023, 12, 30));
        System.out.println(listWithSubscriptionsIsWorkingInDate.toString());

    }
}
