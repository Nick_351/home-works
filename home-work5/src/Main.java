import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите количестко человек: ");
        int numberOfHuman = scan.nextInt();
        String[] allNames = {"Николай", "Иван", "Боб", "Ашот", "Тимур", "Игорь", "Ярополк", "Казимир", "Ибн-аль-Махмед", "Феофан"};
        String[] allLastNames = {"Иванов", "Петров", "Сидоров", "Воробьев", "Соловьев", "Джигурда", "Султангараев", "Кукушкин", "Алибабаев", "Смелый"};
        Human[] hum = new Human[numberOfHuman];

        System.out.println("Массив до сортировки");
        for (int i = 0; i < numberOfHuman; i++) {
            hum[i] = Human.builder()
                    .age((int)(Math.random() * 100))
                    .name(allNames[(int)(Math.random() * 10)])
                    .lastName(allLastNames[(int)(Math.random() * 10)])
                    .build();
            System.out.print("Человек под номером " + i + " в массиве: ");
            System.out.println(hum[i].getName() + " " + hum[i].getLastName() + ", его возраст " + hum[i].getAge());
        }
        Human support;
        for (int j = 0; j < hum.length - 1; j++){
            for(int z = 0; z < hum.length - 1; z++){
                if(hum[z].getAge() > hum[z+1].getAge()){
                    support = hum[z+1];
                    hum[z+1] = hum[z];
                    hum[z] = support;
                }
            }
        }
        System.out.println("Массив после сортировки:");
        for (int d = 0; d < hum.length; d++){
            System.out.print("Человек под номером " + d + " в массиве: ");
            System.out.println(hum[d].getName() + " " + hum[d].getLastName() + ", его возраст " + hum[d].getAge());
        }
    }
}
