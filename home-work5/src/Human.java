import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Human {
    private int age;
    private String name;
    private String lastName;
}
