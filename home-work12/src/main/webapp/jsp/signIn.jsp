<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.09.2023
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SignIn</title>
</head>
<body>
<form action="/signIn" method="post">
<h1>Sign In Page</h1>
<h2>Please Enter Your Data:</h2>
<label for="email">Enter your email</label>
<input id="email" type="email" name="email" placeholder="Your email">
<br>
<label for="password">Enter your password</label>
<input id="password" type="password" name="password" placeholder="Your password">
<input type="submit" value="Sign In!">
</form>
</body>
</html>
