<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.09.2023
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign Up Page</title>
</head>
<body>
<h1>Sign Up Page</h1>
<h2>Please Enter Your Data:</h2>
<form action="/signUp" method="post">
    <label for="firstName">Enter your firstName</label>
    <input id="firstName" name="firstName" placeholder="Your first name">
    <br>
    <label for="lastName">Enter your lastName</label>
    <input id="lastName" name="lastName" placeholder="Your last name">
    <br>
    <label for="email">Enter your email</label>
    <input id="email" type="email" name="email" placeholder="Your email">
    <br>
    <label for="password">Enter your password</label>
    <input id="password" type="password" name="password" placeholder="Your password">
    <input type="submit" value="Sign Up!">
</form>
</body>
</html>
