package servlets;

import dto.SignInForm;
import models.Account;
import repository.AccountsRepository;
import repository.AccountsRepositoryImpl;
import services.SignInService;
import services.SignInServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignInService signInService;

    @Override
    public void init() throws ServletException {
        signInService = new SignInServiceImpl((ArrayList<Account>) AccountsRepositoryImpl.accounts);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignInForm form = SignInForm.builder()
                .email(req.getParameter("email"))
                .password(req.getParameter("password"))
                .build();

        if(signInService.authorization(form)){
            resp.sendRedirect("jsp/profile.jsp");
        }
        else {
            resp.sendRedirect("/signUp");
        }
    }
}
