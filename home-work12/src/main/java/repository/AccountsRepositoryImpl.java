package repository;

import dto.SignInForm;
import dto.SignUpForm;
import models.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountsRepositoryImpl implements AccountsRepository {

    public final static List<Account> accounts = new ArrayList<>();
    private static Long countId = 0L;
    @Override
    public void save(SignUpForm form) {
        Account account = Account.builder()
                .id(countId++)
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();
        System.out.println(account);
        accounts.add(account);
    }
}
