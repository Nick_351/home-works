package repository;

import dto.SignInForm;
import dto.SignUpForm;

public interface AccountsRepository {
    void save(SignUpForm form);
}
