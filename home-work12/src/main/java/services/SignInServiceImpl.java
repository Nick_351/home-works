package services;

import dto.SignInForm;
import models.Account;
import repository.AccountsRepository;
import repository.AccountsRepositoryImpl;

import java.util.ArrayList;

public class SignInServiceImpl implements SignInService{
    private ArrayList<Account> accounts;

    public SignInServiceImpl(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean authorization(SignInForm form) {
        for(Account account: accounts){
            if(account.getEmail().equals(form.getEmail())
                    && account.getPassword().equals(form.getPassword())){
                return true;
            }
        }
        return false;
    }
}
