package services;

import dto.SignInForm;

public interface SignInService {
    public boolean authorization(SignInForm form);
}
