package services;

import models.Student;
import models.Teacher;
import org.hibernate.Session;
import java.util.List;

public class GetDataFromHibernateImpl implements GetDataFromHibernate {

    @Override
    public List<Student> getListStudentsFromDB(Session session) {
        try{
            List<Student> students ;
            students = session.createQuery("SELECT a FROM Student a", Student.class).getResultList();
            System.out.println("-------Список учеников-------");
            for(Student student : students){
                System.out.print(student.getFirstName() + " ");
                System.out.print(student.getPatronymic() + " ");
                System.out.println(student.getLastName());
            }
            return students;
        }
        catch(Exception e)
        {
            throw new RuntimeException();
        }
    }

    @Override
    public List<Teacher> getListTeachersFromDB(Session session) {
        try{
            List<Teacher> teachers ;
            teachers = session.createQuery("SELECT a FROM Teacher a", Teacher.class).getResultList();
            System.out.println("-------Список учителей-------");
            for(Teacher teacher : teachers){
                System.out.print(teacher.getFirstName() + " ");
                System.out.print(teacher.getPatronymic() + " ");
                System.out.println(teacher.getLastName());
            }

            return teachers;
        }
        catch(Exception e)
        {
            throw new RuntimeException();
        }
    }

    @Override
    public void getStudentFromDB(Session session, Long studentID) {
        Student student = session.get(Student.class, studentID);
        System.out.println("Студент " + student.getFirstName() + " " + student.getPatronymic() + " " +
                student.getLastName());
        System.out.println("Его учителя: ");
        for(Teacher teacher : student.getTeachers()){
            System.out.println(teacher.getFirstName() + " " + teacher.getPatronymic() + " " + teacher.getLastName());
        }
    }

    @Override
    public void getTeacherFromDB(Session session, Long teachersID) {
        Teacher teacher = session.get(Teacher.class, teachersID);
        System.out.println(teacher);
    }
}
