package services;

import models.Student;
import models.Teacher;
import org.hibernate.Session;

import java.util.List;

public interface GetDataFromHibernate {
    List<Student> getListStudentsFromDB(Session session);
    List<Teacher> getListTeachersFromDB(Session session);
    void getStudentFromDB(Session session, Long studentID);
    void getTeacherFromDB(Session session, Long teacherID);
}
