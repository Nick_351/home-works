package models;

import lombok.*;

import java.util.List;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Teacher")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "Teacher_Student",
            joinColumns = { @JoinColumn(name = "teacher_id")},
            inverseJoinColumns = { @JoinColumn(name = "student_id")}
    )
    private List<Student> students;
}
