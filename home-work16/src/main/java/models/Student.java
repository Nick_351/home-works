package models;

import lombok.*;

import java.util.List;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Student")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "students")
    private List<Teacher> teachers;
}
