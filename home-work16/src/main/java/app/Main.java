package app;

import models.Student;
import models.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import services.GetDataFromHibernate;
import services.GetDataFromHibernateImpl;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
            Session session = sessionFactory.openSession()){
//            session.getTransaction().begin();
//
//            Student student1 = Student.builder()
//                    .firstName("Иван")
//                    .lastName("Иванов")
//                    .patronymic("Иванович")
//                    .build();
//
//            Student student2 = Student.builder()
//                    .firstName("Петр")
//                    .lastName("Петров")
//                    .patronymic("Петрович")
//                    .build();
//
//            Student student3 = Student.builder()
//                    .firstName("Сергей")
//                    .lastName("Сергеев")
//                    .patronymic("Сергеевич")
//                    .build();
//
//            Student student4 = Student.builder()
//                    .firstName("Борис")
//                    .lastName("Борисов")
//                    .patronymic("Борисович")
//                    .build();
//
//            Teacher teacher1 = Teacher.builder()
//                    .firstName("Антон")
//                    .lastName("Чехов")
//                    .patronymic("Павлович")
//                    .build();
//
//            Teacher teacher2 = Teacher.builder()
//                    .firstName("Лев")
//                    .lastName("Толстой")
//                    .patronymic("Николаевич")
//                    .build();
//
//            Teacher teacher3 = Teacher.builder()
//                    .firstName("Иван")
//                    .lastName("Тургенев")
//                    .patronymic("Сергеевич")
//                    .build();
//
//            Teacher teacher4 = Teacher.builder()
//                    .firstName("Николай")
//                    .lastName("Гоголь")
//                    .patronymic("Васильевич")
//                    .build();
//
//            teacher1.setStudents(Arrays.asList(student1, student4));
//            teacher2.setStudents(Arrays.asList(student1, student2, student3, student4));
//            teacher3.setStudents(Arrays.asList(student2, student3));
//            teacher4.setStudents(Arrays.asList(student2, student3, student4));

//            session.save(student1);
//            session.save(student2);
//            session.save(student3);
//            session.save(student4);
//            session.save(teacher1);
//            session.save(teacher2);
//            session.save(teacher3);
//            session.save(teacher4);
//            session.getTransaction().commit();

            GetDataFromHibernate getDataFromHibernate = new GetDataFromHibernateImpl();

            getDataFromHibernate.getStudentFromDB(session, 2l);
            getDataFromHibernate.getTeacherFromDB(session, 3L);
            getDataFromHibernate.getListStudentsFromDB(session);
            getDataFromHibernate.getListTeachersFromDB(session);

            }
        }
    }
