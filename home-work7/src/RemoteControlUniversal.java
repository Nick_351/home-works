
public class RemoteControlUniversal implements RemoteControl{

    private TV tv;

    @Override
    public void connecting(TV tv) {
        this.tv = tv;
    }

    @Override
    public void poverON() {
        tv.powerOnTV();
    }

    @Override
    public void channelSwitchingByNumbers() {
        tv.doChannelSwitchingByNumbers();
    }

    @Override
    public void channelSwitchingForward() {
        tv.doChannelSwitchingForward();
    }

    @Override
    public void channelSwitchingBack() {
        tv.doChannelSwitchingBack();
    }

    @Override
    public void switchingToTheLastChannel() {
        tv.doSwitchingToTheLastChannel();
    }

}
