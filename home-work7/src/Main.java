public class Main {
    public static void main(String[] args) {

        Channel [] arrayChannels = new Channel[5];

        Program [] arrayProgramsTNT = new Program [6];
        arrayProgramsTNT [0] = Program.builder()
                .programName("Программа на Первом канале - 1")
                .build();
        arrayProgramsTNT [1] = Program.builder()
                .programName("Программа на Первом канале - 2")
                .build();
        arrayProgramsTNT [2] = Program.builder()
                .programName("Программа на Первом канале - 3")
                .build();
        arrayProgramsTNT [3] = Program.builder()
                .programName("Программа на Первом канале - 4")
                .build();
        arrayProgramsTNT [4] = Program.builder()
                .programName("Программа на Первом канале - 5")
                .build();
        arrayProgramsTNT [5] = Program.builder()
                .programName("Программа на Первом канале - 6")
                .build();

        arrayChannels [0] = Channel.builder()
                .channelName("Первый канал")
                .arrayPrograms(arrayProgramsTNT)
                .build();

        Program [] arrayProgramsSTS = new Program [6];
        arrayProgramsSTS [0] = Program.builder()
                .programName("Программа на Втором канале - 1")
                .build();
        arrayProgramsSTS [1] = Program.builder()
                .programName("Программа на Втором канале - 2")
                .build();
        arrayProgramsSTS [2] = Program.builder()
                .programName("Программа на Втором канале - 3")
                .build();
        arrayProgramsSTS [3] = Program.builder()
                .programName("Программа на Втором канале - 4")
                .build();
        arrayProgramsSTS [4] = Program.builder()
                .programName("Программа на Втором канале - 5")
                .build();
        arrayProgramsSTS [5] = Program.builder()
                .programName("Программа на Втором канале - 6")
                .build();

        arrayChannels [1] = Channel.builder()
                .channelName("Второй канал")
                .arrayPrograms(arrayProgramsSTS)
                .build();

        Program [] arrayPrograms1st = new Program [6];
        arrayPrograms1st [0] = Program.builder()
                .programName("Программа на Третьем канале - 1")
                .build();
        arrayPrograms1st [1] = Program.builder()
                .programName("Программа на Третьем канале - 2")
                .build();
        arrayPrograms1st [2] = Program.builder()
                .programName("Программа на Третьем канале - 3")
                .build();
        arrayPrograms1st [3] = Program.builder()
                .programName("Программа на Третьем канале - 4")
                .build();
        arrayPrograms1st [4] = Program.builder()
                .programName("Программа на Третьем канале - 5")
                .build();
        arrayPrograms1st [5] = Program.builder()
                .programName("Программа на Третьем канале - 6")
                .build();

        arrayChannels [2] = Channel.builder()
                .channelName("Третий канал")
                .arrayPrograms(arrayPrograms1st)
                .build();

        Program [] arrayProgramsRussia24 = new Program [6];
        arrayProgramsRussia24  [0] = Program.builder()
                .programName("Программа на Четвертом канале - 1")
                .build();
        arrayProgramsRussia24  [1] = Program.builder()
                .programName("Программа на Четвертом канале - 2")
                .build();
        arrayProgramsRussia24  [2] = Program.builder()
                .programName("Программа на Четвертом канале - 3")
                .build();
        arrayProgramsRussia24  [3] = Program.builder()
                .programName("Программа на Четвертом канале - 4")
                .build();
        arrayProgramsRussia24  [4] = Program.builder()
                .programName("Программа на Четвертом канале - 5")
                .build();
        arrayProgramsRussia24  [5] = Program.builder()
                .programName("Программа на Четвертом канале - 6")
                .build();

        arrayChannels [3] = Channel.builder()
                .channelName("Четвертый канал")
                .arrayPrograms(arrayProgramsRussia24)
                .build();

        RemoteControl remoteControl1 = new RemoteControlUniversal();
        TV tv = new TV(arrayChannels, remoteControl1);

        remoteControl1.poverON();
        remoteControl1.channelSwitchingByNumbers();
        remoteControl1.channelSwitchingByNumbers();
        remoteControl1.channelSwitchingByNumbers();
        remoteControl1.channelSwitchingByNumbers();
        remoteControl1.channelSwitchingForward();
        remoteControl1.channelSwitchingForward();
        remoteControl1.channelSwitchingForward();
        remoteControl1.channelSwitchingBack();
        remoteControl1.channelSwitchingBack();
        remoteControl1.channelSwitchingBack();
        remoteControl1.switchingToTheLastChannel();
        remoteControl1.switchingToTheLastChannel();
        remoteControl1.switchingToTheLastChannel();
        remoteControl1.channelSwitchingByNumbers();
        remoteControl1.switchingToTheLastChannel();
        remoteControl1.switchingToTheLastChannel();

//        RemoteControl remoteControl2 = new RemoteControlByVoice();
//        TV tv = new TV(arrayChannels,remoteControl2);
//        remoteControl2.poverON();
//        remoteControl2.channelSwitchingByNumbers();
//        remoteControl2.switchingToTheLastChannel();
//        remoteControl2.channelSwitchingForward();
//        remoteControl2.channelSwitchingBack();
    }
}
