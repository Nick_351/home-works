import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class Program {
    private String programName;

    public String getProgramName(){
        return programName;
    }
    public void setProgramName(String programName){
        this.programName = programName;
    }

}
