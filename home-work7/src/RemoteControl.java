public interface RemoteControl {

    void connecting(TV tv);
    void poverON();
    void channelSwitchingByNumbers();
    void channelSwitchingForward();
    void channelSwitchingBack();
    void switchingToTheLastChannel();

}
