import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Scanner;
@Builder
@AllArgsConstructor
@Getter
@Setter
public class TV {

    private Channel [] arrayChannels;
    private RemoteControl remoteControl;
    private int currentChannel;
    private int сurrentProgram;
    private int lastChannel;
    private boolean powerTV = false;

    public boolean isPowerTV() {
        return powerTV;
    }

    public TV(Channel [] arrayChannels, RemoteControl remoteControl){
        this.arrayChannels = arrayChannels;
        this.remoteControl = remoteControl;
        remoteControl.connecting(this);
    }

    public void powerOnTV(){
        if(!isPowerTV()){
            setPowerTV(true);
            System.out.println("Телевизор включился");
            doCurrentChanel(getArrayChannels());
            setСurrentProgram(doCurrentProgram(getArrayChannels()));
            setLastChannel(getCurrentChannel());
        }
        else {
            System.out.println("Телевизор уже включен");
        }
    }
    //выбор рандомного канала из массива
    private static int randomChannel (Channel[] channels, int num){
        if(channels[num] == null || num > channels.length){
            num = (int) ((Math.random() * 10) / 2);
        }
        return num;
    }
    //выбор рандомной программы из массива
    private static int randomProgram(Program [] programs, int num){
        if (programs[num] == null || num > programs.length){
            num = (int) ((Math.random() * 10) / 2);
        }
            return num;
        }

    private int doCurrentChanel(Channel [] channels){
        int numberRandomChannel = randomChannel(channels,(int)((Math.random() * 10) / 2));
        System.out.println(channels[numberRandomChannel].getChannelName());
        setCurrentChannel(numberRandomChannel);
        return getCurrentChannel();
    }
    private int doCurrentProgram(Channel [] channels){
        int numberRandomProgram = randomProgram(channels[getCurrentChannel()].getArrayPrograms(),(int)((Math.random() * 10) / 2));
        System.out.println(channels[getCurrentChannel()].getArrayPrograms()[numberRandomProgram].getProgramName());
        setСurrentProgram(numberRandomProgram);
        return getСurrentProgram();
    }
    public void doChannelSwitchingByNumbers (){
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите номер канала ");
        int signal = scan.nextInt();
        if (!isPowerTV()){
            System.out.println("Включите телевизор");
        }
        else if (signal > 0 && signal < getArrayChannels().length){
            setLastChannel(getCurrentChannel());
            setCurrentChannel(signal-1);
            System.out.println(getArrayChannels()[getCurrentChannel()].getChannelName());
            System.out.println(getArrayChannels()[getCurrentChannel()].getArrayPrograms()[getСurrentProgram()].getProgramName());
        }
    }
    public void doChannelSwitchingForward(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите \"0\" для переключения каналов вперед ");
        int signal = scan.nextInt();
        if(signal == 0 && getCurrentChannel() < getArrayChannels().length-1){
            setLastChannel(getCurrentChannel());
            setCurrentChannel(getCurrentChannel()+1);
            if(getCurrentChannel() < getArrayChannels().length-1){
                System.out.println(getArrayChannels()[getCurrentChannel()].getChannelName());
                System.out.println(getArrayChannels()[getCurrentChannel()].getArrayPrograms()[getСurrentProgram()].getProgramName());
            }
        }
        else{
            setCurrentChannel(getCurrentChannel()-1);
            System.out.println("Вы ввели не \"0\", либо такого канала не существует");
        }
    }
    public void doChannelSwitchingBack(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите \"-1\" для переключения каналов назад ");
        int signal = scan.nextInt();
        if(signal == - 1 && getCurrentChannel() > 0){
            setLastChannel(getCurrentChannel());
            setCurrentChannel(getCurrentChannel() - 1);
            System.out.println(getArrayChannels()[getCurrentChannel()].getChannelName());
            System.out.println(getArrayChannels()[getCurrentChannel()].getArrayPrograms()[getСurrentProgram()].getProgramName());
        }
        else{
            System.out.println("Вы ввели не \"-1\", либо такого канала не существует");
        }
    }
    public void doSwitchingToTheLastChannel(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите \"777\" для переключения на включенный ранее канал ");
        int signal = scan.nextInt();
        int bufferLast = getLastChannel();
        int bufferCurrent = getCurrentChannel();

        if(signal == 777){
            setCurrentChannel(bufferLast);
            setLastChannel(bufferCurrent);
            System.out.println(getArrayChannels()[getCurrentChannel()].getChannelName());
            System.out.println(getArrayChannels()[getCurrentChannel()].getArrayPrograms()[getСurrentProgram()].getProgramName());
        }
        else{
            System.out.println("Вы ввели не \"777\", либо такого канала не существует");
        }
    }

}
