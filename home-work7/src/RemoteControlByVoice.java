public class RemoteControlByVoice implements RemoteControl {
    TV tv;

    @Override
    public void connecting(TV tv) {
        this.tv = tv;
    }

    @Override
    public void poverON() {
        tv.powerOnTV();
    }

    @Override
    public void channelSwitchingByNumbers() {
        tv.doChannelSwitchingByNumbers();
        System.out.println("Голосовой пульт для телевизора переключил канал на нужный номер");
    }

    @Override
    public void channelSwitchingForward() {
        tv.doChannelSwitchingForward();
        System.out.println("Голосовой пульт для телевизора включил следующий канал");
    }

    @Override
    public void channelSwitchingBack() {
        tv.doChannelSwitchingBack();
        System.out.println("Голосовой пульт для телевизора включил предыдущий канал");
    }

    @Override
    public void switchingToTheLastChannel() {
        tv.doSwitchingToTheLastChannel();
        System.out.println("Голосовой пульт для телевизора включил последний канал");
    }
}
